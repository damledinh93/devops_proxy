var http = require('http'),
    httpProxy = require('http-proxy'),
    proxy = httpProxy.createProxyServer({}),
    port = 80,
    url = require('url');

http.createServer(function(req, res) {
    var hostname = req.headers.host.split(":")[0];
    var pathname = url.parse(req.url).pathname;

    console.log(hostname);
    console.log(pathname);

    switch(hostname)
    {
        case 'azconverter.com':
            proxy.web(req, res, { target: 'http://localhost:3016' });
            break;
        case 'admin.azconverter.com':
            proxy.web(req, res, { target: 'http://localhost:9999' });
            break;
        case 'jenkins.azconverter.com':
            proxy.web(req, res, { target: 'http://localhost:8080' });
            break;
        default:
            proxy.web(req, res, { target: 'http://localhost:3016' });
    }
}).listen(port, function() {
    console.log('proxy listening on port 80');
});